/* comments are sent by email. this is the comment form */

var who = null;
function g(n) { return document.getElementById(n); }

function bubble(day){
    if (who)
	bubble_close(who);
    document.comment.day.value = day;
    t = g("bubble-"+day).innerHTML
	g("bubble-"+day).innerHTML = g("bubble_form").innerHTML;
    g("bubble_form").innerHTML = t;
    document.comment.email.focus();
    document.comment.email.select();
    who = day;

}
function bubble_close(day){
    //day = document.comment.day.value;
    t = g("bubble-"+day).innerHTML
	g("bubble-"+day).innerHTML = g("bubble_form").innerHTML;
    g("bubble_form").innerHTML = t;
    who = null;
}
