# stream: the weblog killer!
#
# the basic idea behind stream is you select interesting things
# out of your thoughts stream and share it with the world. They
# may or may not be interesting to the target reader at the
# moment, but you keep contributing!
#
# the feed (rss, atom) has 'per day' items. This means all things
# posted that day go in a single feed entry. This, IMO,
# encourages Flow, the mental state.
#
# ..but by all means, cease bragging.

# -*- mode: python -*-

import os, re, datetime, calendar
from time import strptime
import smtplib

from sqlalchemy import *
import logging # this masks sqlalchemy's logging variable

import web
web.webapi.internalerror = web.debugerror

from genshi.template import TemplateLoader
from genshi.core import Markup
from genshi.builder import tag as T


LOG_FILE     = "/home/protected/logs/stream.log"
UTC_OFFSET   = ("+5 hours", "+30 minutes") # adheres to sqlite
SQLITE_CONN  = "sqlite:////home/protected/data/stream.db"
SECRET_FILE  = "~/.stream.secret"
SUBTITLE     = "to extend, wave or float outward, as if in the wind"

logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(levelname)s %(message)s',
                    filename=LOG_FILE)


loader = TemplateLoader(["templates"], auto_reload=True)

def render(name, **namespace):
    "Render the genshi template `name' using `namespace'"
    tmpl = loader.load(name)
    namespace.update(globals()) # we need not pass common methods explicitly
    stream = tmpl.generate(**namespace)
    web.header(
        "Content-Type","%s; charset=utf-8" % namespace.get(
            'content_type', 'text/html'))
    return stream.render(namespace.get('stream_kind', 'html'))



metadata = BoundMetaData(SQLITE_CONN)
posts_table = Table("posts", metadata,
                    Column("post_id", Integer,
                           primary_key=True, autoincrement=True),
                    Column("content", String),
                    Column("time", DateTime,
                           default=func.datetime("now", "UTC",
                                                 *UTC_OFFSET)))
posts_table.create(checkfirst=True)

ONE_DAY = datetime.timedelta(1)

def sqlite_day(d):
    # this is accepted by sqlite
    # YYYY-MM-DD
    # http://www.sqlite.org/cvstrac/wiki?p=DateAndTimeFunctions
    return d.strftime("%Y-%m-%d")


class PostGroup:
    """A PostGroup refers to a day's posts.
.    """

    class NoPosts(Exception): pass

    def __init__(self, day, posts):
        """
        Properties:
        
        `day' is the day corresponding to this post group.
        `posts' is the ordered list of posts belonging to that day.
        """
        self.day = day
        self.posts = posts

    def prev(self):
        prev_post = self.posts[-1].prev()
        if prev_post is None:
            return None

        return PostGroup.for_day(prev_post.day())

    def next(self):
        next_post = self.posts[0].next()
        if next_post is None:
            return None

        return PostGroup.for_day(next_post.day())

    def permalink(self, month_view=False):
        if month_view:
            return link_to("%d/%.2d#%.2d" %
                           (self.day.year, self.day.month, self.day.day))
        else:
            return link_to("%d/%.2d/%.2d" %
                           (self.day.year, self.day.month, self.day.day))

    def id(self):
        "Uniq id for this group"
        return self.day.strftime("%Y%m%d")

    def title(self):
        return self.day.strftime("%A %b %d, %Y")

    def last_updated(self):
        ".. is the time of most recent post in this group"
        def rfc3339_date(date):
            return date.strftime('%Y-%m-%dT%H:%M:%SZ')
        return rfc3339_date(self.posts[0].time)

    @staticmethod
    def months(in_year):
        "Return hash of month => count of post_group's'"
        r = select([func.strftime("%m", posts_table.c.time, "UTC", *UTC_OFFSET),
                    func.count(posts_table.c.post_id)],
                   func.strftime("%Y",posts_table.c.time,
                                 "UTC", *UTC_OFFSET)==str(in_year),
                   engine=metadata.get_engine(),
                   group_by=[func.strftime("%m", posts_table.c.time,
                                           "UTC", *UTC_OFFSET)]
                   ).execute().fetchall()
        count = {}
        for m,c in r:
            count[int(m)] = c
        return count

    @staticmethod
    def years():
        "Return hash of year => count of post_group's'"
        r = select([func.strftime("%Y", posts_table.c.time, "UTC", *UTC_OFFSET),
                    func.count(posts_table.c.post_id)],
                   engine=metadata.get_engine(),
                   group_by=[func.strftime("%Y", posts_table.c.time,
                                           "UTC", *UTC_OFFSET)]
                   ).execute().fetchall()
        count = {}
        for y,c in r:
            count[int(y)] = c
        return count

    @staticmethod
    def recent(limit_days=10, skip_today=False):
        end_date   = Post.last().day()

        # Find the day in which limit_days'th post lies.
        s = select([posts_table.c.time.label("time")],
                   engine=metadata.get_engine(),
                   order_by=[desc(posts_table.c.time)],
                   group_by=[func.strftime("%Y-%m-%d", posts_table.c.time,
                                           "UTC", *UTC_OFFSET)],
                   use_labels=True
                   )
        dates = select([s.c.time],
                       limit=limit_days).execute().fetchall()

        logging.debug("dates are: %s" % dates)
        start_date = datetime.date(dates[-1][0].year,
                                   dates[-1][0].month,
                                   dates[-1][0].day)
        
        if skip_today and end_date == Post.today():
            end_date -= ONE_DAY

        return PostGroup.for_range(start_date, end_date)

    @staticmethod
    def for_range(start_date, end_date):
        """Return all PostGroup's for given range in
        descending order of post time.
        """
        logging.debug("for_range: %s - %s", start_date, end_date)
        query = create_session().query(Post)
        s = sqlite_day

        posts = query.select(and_(posts_table.c.time < s(end_date + ONE_DAY),
                                  posts_table.c.time >= s(start_date)),
                             order_by=[desc(posts_table.c.time)])

        logging.debug("Posts returned: %s" % posts)
        # group them...
        grouped_posts = []
        for post in posts:
            if grouped_posts and post.day() == grouped_posts[-1].day:
                grouped_posts[-1].posts.append(post)
            else:
                grouped_posts.append( PostGroup(post.day(), [post]) )
        
        return grouped_posts

    @staticmethod
    def for_day(day):
        "Return PostGroup for the given day."
        first, = PostGroup.for_range(day, day)
        return first

    
class Post(object):

    def day(self):
        return datetime.date(self.time.year,
                             self.time.month,
                             self.time.day)

    def prev(self):
        "Get the previous post (posted before self)"
        query = create_session().query(Post)
        return query.selectfirst(posts_table.c.time < self.time,
                                 order_by=[desc(posts_table.c.time)])

    def next(self):
        "Get the next post (posted after self)"
        query = create_session().query(Post)
        return query.selectfirst(posts_table.c.time > self.time,
                                 order_by=[asc(posts_table.c.time)])
        
    @staticmethod
    def last():
        "Return the last/recent post"
        query = create_session().query(Post)
        return query.selectfirst(order_by=[desc(posts_table.c.time)])

    @staticmethod
    def first():
        "Return the first/old post"
        query = create_session().query(Post)
        return query.selectfirst(order_by=[asc(posts_table.c.time)])

    @staticmethod
    def today():
        "Return today's date"
        today, = select([func.datetime("now", "UTC", *UTC_OFFSET)],
                        engine=metadata.get_engine()
                        ).execute().fetchone()
        return datetime.date(*strptime(today, "%Y-%m-%d %H:%M:%S")[0:3])


mapper(Post, posts_table)

def match_secret(secret):
    return open(os.path.expanduser(SECRET_FILE)).read().strip() == secret

# HTTP
# ----

urls = (
    '/',                         'index',
    '/(\d{4})/(\d{2})/(\d{2})',  'view_day',
    '/(\d{4})/(\d{2})',          'view_month',
    '/(\d{4})',                  'view_year',
    '/feed',                     'feed',
    '/post/(.*)',                'post',
    '/bubble',                   'bubble',
)

def link_to(*segs):
    # prefixurl is not "the right" way of generating
    # urls, because we need "absolute" urls
    # but anyways...
    # FIXME: root = web.prefixurl()
    root = "http://localhost:8080/" #"http://nearfar.org/stream/"
    return root + "/".join(map(str, segs))

class index:
    def GET(self):
        groups= PostGroup.recent()
        print render("view.html",
                     title="stream",
                     year=None,month=None,day=None,
                     groups=groups,
                     single_group=False)

class view_day:
    def GET(self, year, month, day):
        year, month, day = map(int, [year, month, day])
        g = PostGroup.for_day(datetime.date(year, month, day))
        print render("view.html",
                     title=g.title(),
                     year=year, month=month, day=day,
                     groups=[g],
                     single_group=True)
        
class view_month:
    def GET(self, year, month):
        year, month= map(int, [year, month])
        
        wd, nr_days = calendar.monthrange(year, month)
        end_day = datetime.date(year, month, nr_days)
        start_day = datetime.date(year, month, 1)
        
        groups = PostGroup.for_range(start_day, end_day)
        title = start_day.strftime("%b, %Y")

        print render("view.html",
                     title=title,
                     year=year, month=month, day=None,
                     groups=groups,
                     single_group=False)
        
class view_year:
    def GET(self, year):
        web.header("Content-Type","text/html; charset=utf-8")
        year = int(year)
        count_hash = PostGroup.months(year)
        
        print render("year.html",
                     title=str(year),
                     year=year, month=None, day=None,
                     months=count_hash)

class feed:
    def GET(self):
        """Return atom feed of recent entries (but
        excluding the 'partial today')
        """
        print render("feed.xml",
                     content_type="application/atom+xml",
                     stream_kind="xml",
                     self_href=link_to('feed'),
                     groups=PostGroup.recent(skip_today=True))

class post:
    GET = web.autodelegate('GET_')
    POST = web.autodelegate('POST_')

    def GET_new(self):
        template("edit", content="",
                         id="-1",
                         title_prefix="New Entry")

    def GET_edit(self, post_id):
        post_id = int(post_id[1:])
        session = create_session()
        post = session.query(Post).get_by_post_id(post_id)

        template("edit", content=post.content,
                         id=str(post_id),
                         title_prefix="Edit Entry #%d" % post_id)

    def POST_save(self):
        input = web.input()
        post_id = int(input.id)
        if not match_secret(input.secret):
            return T.p[ "Hmm.. ya cant touch that." ]

        session = create_session()
        
        if post_id == -1:
            # new post
            post = Post()
            post.content = input.content
        else:
            query = session.query(Post)
            post = query.get_by(post_id=post_id)
            post.content = input.content
            
        session.save(post)
        session.flush()

        web.seeother(link_to())

class bubble:
    def POST(self):
        input = web.input()
        date = datetime.date(*strptime(input.day, "%Y%m%d")[0:3]
                              ).strftime("%A %b %d, %Y")
        content = input.content
        email = input.email

        if not self.valid_email(email):
            template(["That was not a valid email address. Sorry."])

        elif send_email(email, ["Sridhar.Ratna+stream@gmail.com"],
                      "[Stream] %s" % date,
                      content):
            template(["Your comment was sent by email. Thanks. ",
                       T.a(href=link_to())["Go back"]
                     ])
        else:
            template(["There was an ", T.b["error"],
                      " sending your comment. I will look into it shortly. ",
                       T.a(href=link_to())["Go back"]
                     ])

    def valid_email(self, email):
        """validate email address
        - http://aspn.activestate.com/ASPN/Cookbook/Python/Recipe/65215
        """
        EMAIL_RE = "^.+\\@(\\[?)[a-zA-Z0-9\\-\\.]+\\.([a-zA-Z]{2,3}|[0-9]{1,3})(\\]?)$"
        if not(len(email) > 7 and re.match(EMAIL_RE, email)):
            return False
        return True


def send_email(from_, toaddrs, subject, body):
    SENDMAIL = "sendmail" # sendmail location
    p = os.popen("%s -t" % SENDMAIL, "w")
    p.write("From: %s\n" % from_)
    p.write("To: %s\n" % ",".join(toaddrs))
    p.write("Subject: %s\n" % subject)
    p.write("\n")
    p.write(body)
    sts = p.close()
    if sts is not None and sts != 0:
        logging.error("sendmail failed with exit status: %d. Message follows,",
                      sts)
        logging.info("From: %s", from_)
        logging.info("Subject: %s", subject)
        logging.info("")
        logging.info(body)
        return False
    return True
